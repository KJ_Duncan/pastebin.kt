plugins {
  kotlin("jvm") version "1.7.10"
  id("org.jetbrains.dokka") version "1.7.10"
}

group = "org.kjd.pastebin.kt"
version = "1.0-SNAPSHOT"

repositories { mavenCentral() }

// As at 2022-09-08
dependencies {
  constraints {
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.6.4")
    implementation("io.ktor:ktor-client-cio:2.1.1")

    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.13.2")
    implementation("org.slf4j:slf4j-simple:2.0.0")
    implementation("io.github.microutils:kotlin-logging-jvm:2.1.23")

    implementation("com.natpryce:result4k:2.0.0")
    implementation("com.apurebase:arkenv:3.3.3")

    testImplementation("org.junit.jupiter:junit-jupiter:5.9.0")
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.9.0")

    testImplementation("io.kotest:kotest-runner-junit5:5.4.2")
    testImplementation("io.kotest:kotest-assertions-core:5.4.2")

    testImplementation("io.ktor:ktor-client-mock:2.1.1")
    testImplementation("io.ktor:ktor-client-mock-jvm:2.1.1")
  }

  dependencies {
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core")
    implementation("io.ktor:ktor-client-cio")

    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("org.slf4j:slf4j-simple")
    implementation("io.github.microutils:kotlin-logging-jvm")

    implementation("com.natpryce:result4k")
    implementation("com.apurebase:arkenv")

    testImplementation("org.junit.jupiter:junit-jupiter")
    testImplementation("org.junit.jupiter:junit-jupiter-api")

    testImplementation("io.kotest:kotest-runner-junit5")
    testImplementation("io.kotest:kotest-assertions-core")

    testImplementation("io.ktor:ktor-client-mock")
    testImplementation("io.ktor:ktor-client-mock-jvm")
  }
}

tasks {
  compileKotlin {
    kotlinOptions.jvmTarget = "18"
    kotlinOptions.freeCompilerArgs =
      listOf("-Xallow-no-source-files",
             "-Xinline-classes",
             "-opt-in=kotlin.time.ExperimentalTime",
             "-opt-in=kotlin.io.path.ExperimentalPathApi")
  }

  compileTestKotlin { kotlinOptions.jvmTarget = "18" }

  test {
    useJUnitPlatform()
    jvmArgs = listOf("-enableassertions", "-Dkotlinx.coroutines.debug")
    testLogging.showStandardStreams = true
    testLogging {
      events("passed",
             "failed",
             "standard_out",
             "standard_error")
    }
  }

  // dokka {
  //   outputFormat = "html"
  //   outputDirectory = "$buildDir/javadoc"
  //   configuration { includeNonPublic = true }
  // }

  jar {
    manifest {
      attributes(
        "Manifest-Version" to "1.0",
        "Created-By" to "kjd016 (Kirk Duncan)",
        "Name" to "org/kjd/pastebinkt/",
        "Sealed" to "true",
        "Application-Name" to project.name,
        "Implementation-Title" to "PastebinKt a Kotlin Pastebin.com API",
        "Implementation-Version" to project.version
      )
    }
  }
}

// val dokkaJar: Jar by tasks.creating(Jar::class) {
//   group = JavaBasePlugin.DOCUMENTATION_GROUP
//   description = "Assembles PastebinApp docs with Dokka"
//   archiveClassifier.set("javadoc")
//   from(tasks.dokka)
// }

