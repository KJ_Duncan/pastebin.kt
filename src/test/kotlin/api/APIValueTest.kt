package api

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test

internal class APIValueTest {

  @Test
  fun `user key is not null and value is mate`() {
    val userKey = UserName("mate")
    assertNotNull(userKey)
    assertEquals("mate", userKey.value)
  }

  @Test
  fun `password is not null and value is secrete`() {
    val password = Password("secrete")
    assertNotNull(password)
    assertEquals("secrete", password.value)
  }

  @Test
  fun `sessionId is not null and value is xxx-yyy`() {
    val sessionId = SessionId("xxx-yyy")
    assertNotNull(sessionId)
    assertEquals("xxx-yyy", sessionId.value)
  }

  @Test
  fun `developerKey is not null and value is www-zzz`() {
    val developerKey = DeveloperKey("www-zzz")
    assertNotNull(developerKey)
    assertEquals("www-zzz", developerKey.value)
  }

  @Test
  fun `limit is not null and value is 100`() {
    val limit = Limit("100")
    assertNotNull(limit)
    assertEquals("100", limit.value)
  }

  @Test
  fun `pasteCode is not null and value is new paste`() {
    val pasteCode = PasteCode("new paste")
    assertNotNull(pasteCode)
    assertEquals("new paste", pasteCode.value)
  }

  @Test
  fun `pasteName is not null and value is example`() {
    val pasteName = PasteName("example")
    assertNotNull(pasteName)
    assertEquals("example", pasteName.value)
  }

  @Test
  fun `pasteKey is not null and value is x123y`() {
    val pasteKey = PasteKey("x123y")
    assertNotNull(pasteKey)
    assertEquals("x123y", pasteKey.value)
  }

  @Test
  fun `pasteHits is not null and value is 12`() {
    val pasteHits = PasteHits("12")
    assertNotNull(pasteHits)
    assertEquals("12", pasteHits.value)
  }

  @Test
  fun `scrapeUrl is not null and value is 0CeaNm8Y`() {
    val scrapeUrl = ScrapeUrl("php?i=0CeaNm8Y")
    assertNotNull(scrapeUrl)
    assertEquals("php?i=0CeaNm8Y", scrapeUrl.value)
  }

  @Test
  fun `fullUrl is not null and value is com0CeaNm8Y`() {
    val fullUrl = FullUrl("com0CeaNm8Y")
    assertNotNull(fullUrl)
    assertEquals("com0CeaNm8Y", fullUrl.value)
  }

  @Test
  fun `pasteSize is not null and value is 890`() {
    val pasteSize = PasteSize("890")
    assertNotNull(pasteSize)
    assertEquals("890", pasteSize.value)
  }

  @Test
  fun `pasteDate is not null and value is 1442911665`() {
    val pasteDate = PasteDate("1442911665")
    assertNotNull(pasteDate)
    assertEquals("1442911665", pasteDate.value)
  }
}
