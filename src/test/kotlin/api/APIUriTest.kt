package api

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class APIUriTest {

  @Test
  fun `uri raw is equal to api_raw dot php `() {
    assertEquals("pastebin.com/api/api_raw.php", APIUri.RAW.uri)
  }

  @Test
  fun `uri post is equal to api_post dot php`() {
    assertEquals("pastebin.com/api/api_post.php", APIUri.POST.uri)
  }

  @Test
  fun `uri login is equal to api_login dot php`() {
    assertEquals("pastebin.com/api/api_login.php", APIUri.LOGIN.uri)
  }

  @Test
  fun `uri scraping is equal to api_scraping dot php`() {
    assertEquals("scrape.pastebin.com/api_scraping.php", APIUri.RECENT_SCRAPE.uri)
  }
}
