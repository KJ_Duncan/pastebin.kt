package optional

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test

internal class PasteExpireTest {

  @Test
  fun `paste expire NEVER value equals N`() {
    assertEquals("N", PasteExpire.NEVER.value)
  }

  @Test
  @Disabled("function expires(int) no usage")
  fun `paste expire EXPIRE_N expireDate 0 equals EXPIRE_N`() {
    assertEquals(PasteExpire.NEVER, PasteExpire.NEVER.expires(0))
  }

  @Test
  @Disabled("function expires(int) no usage")
  fun `paste expire function expires equals ONE_MONTH`() {
    assertEquals(PasteExpire.ONE_MONTH, PasteExpire.NEVER.expires(2))
  }

  @Test
  @Disabled("function lookup(String) check usage")
  fun `paste expire function lookup`() {
  }
}
