package optional

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test

internal class PasteVisibilityTest {

  @Test
  fun `paste visibility PUBLIC value is 0`() {
    assertEquals("0", PasteVisibility.PUBLIC.value)
  }

  @Test
  @Disabled("function lookup(String) check usage")
  fun `paste visibility function lookup`() {
  }
}
