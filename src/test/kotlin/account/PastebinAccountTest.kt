package account

import api.DeveloperKey
import api.Password
import api.UserName
import domain.PastebinAccount
import com.natpryce.get
import com.natpryce.onFailure
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Disabled

internal class PastebinAccountTest {

  @Test
  @Disabled
  fun login(): Unit = testBlocking {
  }

  @Test
  @Disabled
  fun getPastes() {
  }

  @Test
  @Disabled
  fun getAccountDetails() {
  }

  @Test
  @Disabled
  fun testGetPastes() {
  }

  @Test
  @Disabled
  fun getLatestPaste() {
  }

  @Test
  @Disabled
  fun kakShow() {
  }

  @Test
  @Disabled
  fun getDevKey() {
  }

  @Test
  @Disabled
  fun getSessionId() {
  }

  private fun testBlocking(callback: suspend () -> Unit): Unit = run { runBlocking { callback() } }
}
