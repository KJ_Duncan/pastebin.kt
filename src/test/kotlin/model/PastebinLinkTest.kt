package model

import java.net.URL

import com.natpryce.valueOrNull

import io.kotest.matchers.types.shouldBeInstanceOf
import io.kotest.assertions.throwables.shouldThrow

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test

internal class PastebinLinkTest {

  private val pastebinLink = PastebinLink("https://pastebin.com/UIFdu235s")

  @Test
  fun `getLink should be an instance of URL`() {
    pastebinLink.getLink().shouldBeInstanceOf<URL>()
  }

  @Test
  fun `getLink Url should match pastebin com`() {
    assertEquals("pastebin.com", pastebinLink.getLink().host)
  }

  @Test
  fun `getLink path equals UIFdu235s`() {
    assertEquals("/UIFdu235s", pastebinLink.getLink().path)
  }

  @Test
  fun `getLink ref is null`() {
    assertNull(pastebinLink.getLink().ref)
  }

  @Test
  fun `getLink content should throw FileNotFoundException`() {
    shouldThrow<java.io.FileNotFoundException> { pastebinLink.getLink().content }
  }

  @Test
  fun `getLink query is null`() {
    assertNull(pastebinLink.getLink().query)
  }

  @Test
  fun `getLink port equals -1`() {
    assertEquals(-1, pastebinLink.getLink().port)
  }

  @Test
  fun `getLink default port equals 443`() {
    assertEquals(443, pastebinLink.getLink().defaultPort)
  }

  @Test
  fun `getLink protocol equals https)`() {
    assertEquals("https", pastebinLink.getLink().protocol)
  }

  @Test
  fun `getLink authority equals pastebin dot com`() {
    assertEquals("pastebin.com", pastebinLink.getLink().authority)
  }

  @Test
  fun `getKey equals PasteKey(value=UIFdu235s)`() {
    assertEquals("PasteKey(value=UIFdu235s)", pastebinLink.getKey().valueOrNull().toString())
  }
}
