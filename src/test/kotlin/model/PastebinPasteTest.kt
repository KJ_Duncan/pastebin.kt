package model

import api.DeveloperKey
import api.PasteCode
import api.SessionId

import io.kotest.matchers.string.shouldContain

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test

internal class PastebinPasteTest {

  private val pastebinPaste: PastebinPaste =
    PastebinPaste(DeveloperKey("xyz"), PasteCode("new paste"), SessionId("123"))

  @Test
  fun `getPasteCode equals PasteCode(value=new paste)`() {
    assertEquals("PasteCode(value=new paste)", pastebinPaste.pasteCode.toString())
  }

  @Test
  fun `getPastesName equals PasteName(value=PastebinAppKt)`() {
    assertEquals("PasteName(value=PastebinAppKt)", pastebinPaste.pasteName.toString())
  }

  @Test
  fun `getPastesFormat equals kotlin`() {
    assertEquals("kotlin", pastebinPaste.pasteFormat.value)
  }

  @Test
  fun `getPastesVisibility equals public value 0`() {
    assertEquals("0", pastebinPaste.pasteVisibility.value)
  }

  @Test
  fun `getPastesExpireDate equals 10M`() {
    assertEquals("10M", pastebinPaste.pasteExpire.toString())
  }

  @Test
  @Disabled("private sessionId not readable")
  fun `toString should contain SessionId(value=123)`() {
    pastebinPaste.toString().shouldContain("SessionId(value=123)")
  }

  @Test
  @Disabled("WebIO call needs to be mocked")
  fun `paste WebIO getContents call`() {
  }

  @Test
  @Disabled("Exceptions need to be tested")
  fun `paste exceptions call`() {
  }
}
