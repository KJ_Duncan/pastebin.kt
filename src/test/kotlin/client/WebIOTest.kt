package client

import api.APIKey
import api.APIUri
import api.DeveloperKey
import api.Password
import api.PasteCode
import api.SessionId
import api.UserName
import optional.PasteFormat

import io.ktor.client.call.body
import io.ktor.client.engine.mock.MockEngine
import io.ktor.client.engine.mock.respond
import io.ktor.client.engine.mock.respondBadRequest
import io.ktor.client.engine.mock.respondError
import io.ktor.client.engine.mock.respondOk
import io.ktor.client.HttpClient
import io.ktor.client.request.get
import io.ktor.client.request.post
import io.ktor.client.request.request
import io.ktor.client.request.url
import io.ktor.client.statement.bodyAsText
import io.ktor.client.statement.readBytes
import io.ktor.http.ContentType
import io.ktor.http.fullPath
import io.ktor.http.headersOf
import io.ktor.http.hostWithPort
import io.ktor.http.HttpStatusCode
import io.ktor.http.Url
import io.ktor.http.URLBuilder
import io.ktor.http.URLProtocol

import kotlinx.coroutines.runBlocking

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test

internal class WebIOTest {

  private val Url.hostWithPortIfRequired: String get() = if (port == protocol.defaultPort) host else hostWithPort
  private val Url.fullUrl: String get() = "${protocol.name}://$hostWithPortIfRequired$fullPath"

  @Test
  fun `test client mock`(): Unit = runBlocking {
    // https://github.com/ktorio/ktor/blob/master/ktor-client/ktor-client-mock/jvm/test/io/ktor/client/tests/mock/MockEngineTests.kt
    val client = HttpClient(MockEngine) {
      engine {
        addHandler { request ->
          if (request.url.encodedPath == "/") return@addHandler respond(
            byteArrayOf(1, 2, 3), headers = headersOf("X-MyHeader", "My Value")
          )

          return@addHandler respondError(HttpStatusCode.NotFound, "Not Found ${request.url.encodedPath}")
        }
      }
      expectSuccess = false
    }

    assertEquals(byteArrayOf(1, 2, 3).toList(), client.get("/").readBytes().toList())
    assertEquals("My Value", client.get("/").headers["X-MyHeader"])
    assertEquals("Not Found /other/path", client.get("/other/path").bodyAsText())
  }

  @Test
  fun `test get post contents`(): Unit = testBlocking {
    val client = HttpClient(MockEngine) {
      engine {
        addHandler { request ->
          when (request.url.fullUrl) {
            "${APIUri.SCHEME.uri}://${APIUri.POST.uri}?api_dev_key=value&api_user_key=value&api_user_name=value&api_paste_format=kotlin&api_paste_code=value" -> {
              val responseHeaders = headersOf("Content-Type" to listOf(ContentType.Text.Plain.toString()))
              respond("APIUri: ${request.url}\nBODY: ${request.body}", headers = responseHeaders)
            }
            else -> error("Unhandled ${request.url.fullUrl}")
          }
        }
      }
      expectSuccess = false
    }

    // "https://pastebin.com/api/api_post.php"
    fun urlBuilder(apiUri: APIUri): Url {
      return URLBuilder(
        protocol = URLProtocol.HTTPS,
        host = apiUri.uri,
        port = URLProtocol.HTTPS.defaultPort,
        pathSegments = listOf<String>(),
        parameters = PostUtils.getPostParameters(),
        fragment = "",
        user = "",
        password = "",
        trailingQuery = false
      ).build()
    }

    PostUtils.clear()
    PostUtils.put(APIKey.DEV_KEY, DeveloperKey("value"))
    PostUtils.put(APIKey.USER_KEY, SessionId("value"))
    PostUtils.put(APIKey.USER_NAME, UserName("value"))
    PostUtils.put(APIKey.PASTE_FORMAT, PasteFormat.FORMAT_KOTLIN)
    PostUtils.put(APIKey.PASTE_CODE, PasteCode("value"))

    val post = client.post(urlBuilder(APIUri.POST))

    assertEquals("HttpResponse[https://:@pastebin.com/api/api_post.php?api_dev_key=value&api_user_key=value&api_user_name=value&api_paste_format=kotlin&api_paste_code=value, 200 OK]", post.toString())
  }

  @Test
  fun `test get login contents`(): Unit = testBlocking {
    val client = HttpClient(MockEngine) {
      engine {
        addHandler { request ->
          when (request.url.fullUrl) {
            "${APIUri.SCHEME.uri}://${APIUri.LOGIN.uri}?api_dev_key=value&api_user_name=value&api_user_password=value" -> {
              val responseHeaders = headersOf("Content-Type" to listOf(ContentType.Text.Plain.toString()))
              respond("APIUri: ${request.url}\nBODY: ${request.body}", headers = responseHeaders)
            }
            else -> error("Unhandled ${request.url.fullUrl}")
          }
        }
      }
      expectSuccess = false
    }

    // "https://pastebin.com/api/api_post.php"
    fun urlBuilder(apiUri: APIUri): Url {
      return URLBuilder(
        protocol = URLProtocol.HTTPS,
        host = apiUri.uri,
        port = URLProtocol.HTTPS.defaultPort,
        pathSegments = listOf<String>(),
        parameters = PostUtils.getPostParameters(),
        fragment = "",
        user = "",
        password = "",
        trailingQuery = false
      ).build()
    }

    PostUtils.clear()
    PostUtils.put(APIKey.DEV_KEY, DeveloperKey("value"))
    PostUtils.put(APIKey.USER_NAME, UserName("value"))
    PostUtils.put(APIKey.USER_PASSWORD, Password("value"))

    val login = client.post(urlBuilder(APIUri.LOGIN))

    assertEquals("HttpResponse[https://:@pastebin.com/api/api_login.php?api_dev_key=value&api_user_name=value&api_user_password=value, 200 OK]", login.toString())
  }

  @Test
  @Disabled
  fun getContents(): Unit = testBlocking {
    val client = HttpClient(MockEngine) {
      engine {
        addHandler { request ->
          when (request.url.fullUrl) {
            "http://localhost/${APIUri.POST.uri}" -> {
              val responseHeaders = headersOf("Content-Type" to listOf(ContentType.Text.Plain.toString()))
              respond("Hello, world", headers = responseHeaders)
            }
            else -> error("Unhandled ${request.url.fullUrl}")
          }
        }
      }
      expectSuccess = false
    }

    assertEquals("Hello, world", client.get(APIUri.POST.uri).body<String>())
    assertEquals("text/plain", client.get(APIUri.POST.uri).headers["Content-Type"])
  }

  @Test
  @Disabled
  fun getBasic() = testBlocking {
    val client = HttpClient(MockEngine { request ->
      if (request.url.toString().endsWith("/fail")) {
        respondBadRequest()
      } else {
        respondOk("${request.url}")
      }
    }) {
      expectSuccess = false
    }

    val message = client.request { url("http://127.0.0.1/normal-request") }
    assertEquals("HttpResponse[http://127.0.0.1/normal-request, 200 OK]", message.toString())
  }

  private fun testBlocking(callback: suspend () -> Unit): Unit = run { runBlocking { callback() } }
}
