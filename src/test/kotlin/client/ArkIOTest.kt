package client

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test

internal class ArkIOTest {

  @Test
  fun `ArkIO is not null`() {
    assertNotNull(ArkIO)
  }

  @Test
  @Disabled
  fun getDevKey() {
  }

  @Test
  @Disabled
  fun getUserName() {
  }

  @Test
  @Disabled
  fun getPassword() {
  }

  @Test
  fun `get kak session`() {
    assertNull(ArkIO.session)
    val args = arrayOf("--API_DEV_KEY", "xyz", "-s", "kak-session")
    ArkIO.parseArguments(args)
    assertNotNull(ArkIO.session)
    assertEquals("kak-session", ArkIO.session)
  }

  @Test
  fun `get paste title`() {
    assertNull(ArkIO.title)
    val args = arrayOf("--API_DEV_KEY", "xyz", "-t", "junit")
    ArkIO.parseArguments(args)
    assertNotNull(ArkIO.title)
    assertEquals("junit", ArkIO.title)
  }

  @Test
  @Disabled
  fun getFormat() {
  }

  @Test
  @Disabled
  fun getVisible() {
  }

  @Test
  @Disabled
  fun getExpire() {
  }

  @Test
  @Disabled
  fun getBuilder() {
  }

  @Test
  @Disabled
  fun getResults() {
  }

  @Test
  @Disabled
  fun getDelete() {
  }

  @Test
  @Disabled
  fun getLogin() {
  }

  @Test
  @Disabled
  fun getUserDetails() {
  }

  @Test
  @Disabled
  fun getShow() {
  }

  @Test
  @Disabled
  fun getScrape() {
  }

  @Test
  @Disabled
  fun getMainString() {
  }
}
