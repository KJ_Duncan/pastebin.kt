package client

import api.APIKey
import api.PasteKey
import optional.PasteExpire
import optional.PasteFormat
import optional.PasteVisibility

import com.natpryce.failureOrNull
import com.natpryce.onFailure
import com.natpryce.valueOrNull

import io.ktor.http.Parameters

import io.kotest.assertions.throwables.shouldNotThrow
import io.kotest.matchers.nulls.shouldBeNull
import io.kotest.matchers.types.shouldBeInstanceOf

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Order
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestMethodOrder

internal class UtilsTest {

  @Nested
  @TestMethodOrder(OrderAnnotation::class)
  inner class PostUtilsTest {

    @Test
    @Order(1)
    fun `post is not null`() {
      assertNotNull(PostUtils)
      PostUtils.clear()
    }

    @Test
    @Order(2)
    fun `getPost should not throw exception and be null on empty list`() {
      shouldNotThrow<Exception> { PostUtils.getPost().onFailure { throw it.reason } }
      PostUtils.getPost().failureOrNull().shouldBeNull()
    }

    @Test
    @Order(3)
    fun `put returns empty string and getPost matches api_paste_key=0CeaNm8Y`() {
      assertEquals("", PostUtils.put(APIKey.PASTE_KEY, PasteKey("0CeaNm8Y")))
      assertEquals("api_paste_key=0CeaNm8Y", PostUtils.getPost().valueOrNull())
    }

    @Test
    @Order(4)
    fun `getPost returns api_paste_key=0CeaNm8Y&api_paste_expire_date=N`() {
      // Success(value=api_paste_expire_date=N)
      assertEquals("", PostUtils.put(APIKey.PASTE_EXPIRES, PasteExpire.NEVER))
      assertEquals("api_paste_key=0CeaNm8Y&api_paste_expire_date=N", PostUtils.getPost().valueOrNull())
    }

    @Test
    @Order(5)
    fun `getPost returns api_paste_key=0CeaNm8Y&api_paste_expire_date=N&api_paste_private=0&api_paste_format=kotlin`() {
      assertEquals("", PostUtils.put(APIKey.PASTE_PRIVATE, PasteVisibility.PUBLIC))
      assertEquals("", PostUtils.put(APIKey.PASTE_FORMAT, PasteFormat.FORMAT_KOTLIN))
      assertEquals(
        "api_paste_key=0CeaNm8Y&api_paste_expire_date=N&api_paste_private=0&api_paste_format=kotlin",
        PostUtils.getPost().valueOrNull()
      )
    }

    @Test
    @Order(6)
    fun `getPostParameters returns same instance as Parameters`() {
      PostUtils.getPostParameters().shouldBeInstanceOf<Parameters>()
    }

    @Test
    @Order(7)
    fun `getPostParameters returns Parameters api_paste_key=0CeaNm8Y api_paste_expire_date=N api_paste_private=0 api_paste_format=kotlin`() {
      // Parameters [api_paste_key=[0CeaNm8Y], api_paste_expire_date=[N], api_paste_private=[0], api_paste_format=[kotlin]]
      assertEquals(
        "Parameters [api_paste_key=[0CeaNm8Y], api_paste_expire_date=[N], api_paste_private=[0], api_paste_format=[kotlin]]",
        PostUtils.getPostParameters().toString()
      )
    }
  }

  @Nested
  inner class DOMUtilsTest {

    @Test
    @Disabled
    fun `getDocument String APITag`() {
    }
  }

  @Nested
  inner class XMLUtilsTest {

    @Test
    @Disabled
    fun `getText Element UserElement`() {
    }

    @Test
    @Disabled
    fun `getText Element PasteElement`() {
    }
  }
}
