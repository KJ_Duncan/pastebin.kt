package exception

import io.ktor.http.HttpStatusCode

import io.kotest.matchers.types.shouldBeInstanceOf

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class PostExceptionTest {

  @Test
  fun `test login exception`() {
    val loginex = LoginException("test login", IllegalArgumentException())
    assertEquals("test login", loginex.message)
    loginex.cause.shouldBeInstanceOf<IllegalArgumentException>()
  }

  @Test
  fun `test parse exception`() {
    val parsex = ParseException("test parse", IllegalStateException())
    assertEquals("test parse", parsex.message)
    parsex.cause.shouldBeInstanceOf<IllegalStateException>()
  }

  @Test
  fun `test paste exception`() {
    val pastex = PasteException("test paste", IllegalArgumentException())
    assertEquals("test paste", pastex.message)
    pastex.cause.shouldBeInstanceOf<IllegalArgumentException>()
  }

  @Test
  fun `test post exception`() {
    val postex = PostException("test post", IllegalArgumentException())
    assertEquals("test post", postex.message)
    postex.cause.shouldBeInstanceOf<IllegalArgumentException>()
  }

  @Test
  fun `test pastebin error`() {
    val pastebiner = PastebinError("test request", HttpStatusCode.Companion.BadRequest, "test pastebin", IllegalAccessError())
    assertEquals("test request", pastebiner.request)
    assertEquals(400, pastebiner.code.value)
    assertEquals("test pastebin", pastebiner.message)
    pastebiner.cause.shouldBeInstanceOf<IllegalAccessError>()
  }
}
