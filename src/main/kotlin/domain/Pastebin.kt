package domain

import api.APIScrape
import api.APIUri
import api.DeveloperKey
import api.FullUrl
import api.Limit
import api.PasteCode
import api.PasteDate
import api.PasteKey
import api.PasteName
import api.PasteSize
import api.ScrapeUrl
import api.SessionId
import api.UserName
import client.WebIO
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.natpryce.Failure
import com.natpryce.Result
import com.natpryce.Success
import com.natpryce.onFailure
import com.natpryce.partition
import com.natpryce.resultFrom
import exception.ParseException
import model.PastebinLink
import model.PastebinPaste
import model.PastebinScrape
import optional.PasteExpire
import optional.PasteFormat
import optional.PasteVisibility
import java.net.URL

class Pastebin {
  /**
   * Include all the following POST parameters when you request the url:
   * 1. api_dev_key - which is your unique API Developers Key.
   * 2. api_option - set as paste, this will indicate you want to create a new paste.
   * 3. api_paste_code - this is the text that will be written inside your paste.
   *
   * These parameters are not required when you create a new paste, but are possible to add:
   * 1. api_user_key - this parameter is part of the login system, which is explained further down the page.  // sessionId
   * 2. api_paste_name - this will be the name / title of your paste.
   * 3. api_paste_format - this will be the syntax highlighting value, which is explained in detail further down the page.
   * 4. api_paste_private - this makes a paste public, unlisted or private, public = 0, unlisted = 1, private = 2
   * 5. api_paste_expire_date - this sets the expiration date of your paste, the values are explained further down the page.
   */
  suspend fun paste(
    developerKey: DeveloperKey,
    pasteCode: PasteCode,
    sessionId: SessionId,
    pasteName: PasteName,
    pasteFormat: PasteFormat,
    pasteVisibility: PasteVisibility,
    pasteExpire: PasteExpire
  ): Result<URL, Exception> {
    val pastebinLink: PastebinLink =
      newPaste(developerKey, pasteCode, sessionId, pasteName, pasteFormat, pasteVisibility, pasteExpire)
        .paste()
        .onFailure { return it }

    return Success(pastebinLink.getLink())
  }

  /**
   * If you are trying to read ALL new public pastes, we recommend that you list 1x per minute
   * the 100 most recent pastes. Store all those ID's/Keys locally somewhere, then fetch all
   * those pastes and process the information however you like.
   *
   * You can limit the results by adding ?limit=10 for example. The max allowed value there is 250.
   *
   * [
   *   {
   *     "scrape_url": "https://scrape.pastebin.com/api_scrape_item.php?i=0CeaNm8Y",
   *     "full_url": "https://pastebin.com/0CeaNm8Y",
   *     "date": "1442911802",
   *     "key": "0CeaNm8Y",
   *     "size": "890",
   *     "expire": "1442998159",
   *     "title": "Once we all know when we goto function",
   *     "syntax": "java",
   *     "user": "admin"
   *   }
   * ]
   */
  suspend fun getScrapingContents(limit: Limit): Result<List<PastebinScrape>, Exception> {
    if (limit.value.toInt() !in 1..250) {
      APIUri.toAPIUri("${APIUri.RECENT_SCRAPE.uri}?i=50")
    } else {
      APIUri.toAPIUri("${APIUri.RECENT_SCRAPE.uri}?i=${limit.value}")
    }

    val response: String = WebIO.getContents(APIUri.toAPIUri).onFailure { return it }

    if (response.isEmpty() || !(response[0] == '[' && response[response.length - 2] == ']')) {
      return Failure(ParseException("Failed to parse pastes: $response", IllegalStateException()))
    }

    val pastes = mutableListOf<Result<PastebinScrape, Exception>>()
    val nodes: JsonNode = getJSonData(response)

    nodes.fields().forEach { node: MutableMap.MutableEntry<String, JsonNode> ->
      val pastebinScrape: Result<PastebinScrape, Exception> = resultFrom {
        PastebinScrape(
          scrapeUrl = ScrapeUrl(node.value.get(APIScrape.SCRAPE_URL.value).toString()),
          fullUrl = FullUrl(node.value.get(APIScrape.FULL_URL.value).toString()),
          pasteDate = PasteDate(node.value.get(APIScrape.DATE.value).toString()),
          pasteKey = PasteKey(node.value.get(APIScrape.KEY.value).toString()),
          pasteSize = PasteSize(node.value.get(APIScrape.SIZE.value).toString()),
          expireDate = PasteExpire.valueOf(node.value.get(APIScrape.EXPIRE.value).toString()),
          pasteName = PasteName(node.value.get(APIScrape.TITLE.value).toString()),
          pasteFormat = PasteFormat.valueOf(node.value.get(APIScrape.SYNTAX.value).toString()),
          pasteUserName = UserName(node.value.get(APIScrape.USER.value).toString())
        )
      }
      pastes.add(pastebinScrape)
    }
    return Success(pastes.partition().first)
  }

  /**
   * To fetch the RAW data of any paste, you can query the URL below.
   * Replace UNIQUE_PASTE_KEY with the key of the paste that you want to fetch.
   */
  suspend fun getScrapeItemContents(pasteKey: PasteKey): Result<String, Exception> {
    APIUri.toAPIUri("${APIUri.RAW_SCRAPE.uri}?i=${pasteKey.value}")
    return WebIO.getContents(APIUri.toAPIUri)
  }

  /**
   * To fetch the RAW data of any paste, you can query the URL below.
   * Replace UNIQUE_PASTE_KEY with the key of the paste that you want to fetch.
   */
  suspend fun getScrapeItemMetaContents(pasteKey: PasteKey): Result<String, Exception> {
    APIUri.toAPIUri("${APIUri.META_SCRAPE.uri}?i=${pasteKey.value}")
    return WebIO.getContents(APIUri.toAPIUri)
  }

  private fun getJSonData(root: String): JsonNode {
    val mapper = ObjectMapper()
    return mapper.readTree(root)
  }

  private fun newPaste(
    developerKey: DeveloperKey,
    pasteCode: PasteCode,
    sessionId: SessionId,
    pasteName: PasteName,
    pasteFormat: PasteFormat,
    pasteVisibility: PasteVisibility,
    pasteExpire: PasteExpire
  ): PastebinPaste {
    return PastebinPaste(developerKey, pasteCode, sessionId, pasteName, pasteFormat, pasteVisibility, pasteExpire)
  }
}
