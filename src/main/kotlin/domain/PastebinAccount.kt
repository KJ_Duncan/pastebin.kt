package domain

import api.APIKey
import api.APIOption
import api.APITag
import api.APIUri
import api.DeveloperKey
import api.Limit
import api.Password
import api.PasteElement
import api.PasteKey
import api.SessionId
import api.UserName
import api.UserPaste
import api.checkLimit
import client.DOMUtils
import client.PostUtils
import client.WebIO
import client.XMLUtils
import com.natpryce.Failure
import com.natpryce.Result
import com.natpryce.Success
import com.natpryce.onFailure
import exception.LoginException
import exception.ParseException
import model.PastebinAccountDetails
import org.w3c.dom.Element
import org.w3c.dom.Node
import org.w3c.dom.NodeList

/**
 * Represents an account on: http://pastebin.com/
 * A reference manual for generating a user session id can be found: http://pastebin.com/api#8
 */
class PastebinAccount(
  private val developerKey: DeveloperKey,
  private val userName: UserName,
  private val password: Password
) {
  var sessionId: SessionId = SessionId("")
  /**
   * Possible Good API Responses: (example)
   *   6c6d3fe13b19bbd6e479b705df0a607f
   *
   * Possible Bad API Responses:
   *   Bad API request, use POST request, not GET
   *   Bad API request, invalid api_dev_key
   *   Bad API request, invalid login
   *   Bad API request, account not active
   *   Bad API request, invalid POST parameters
   */
  suspend fun login(): Result<String, Exception> =
    when {
      checkAccountDetails() -> {
        Failure(LoginException("Already logged in or missing userName details", IllegalStateException()))
      }
      PostUtils.isSuccess {
        PostUtils.clear()
        PostUtils.put(APIKey.DEV_KEY, developerKey)
        PostUtils.put(APIKey.USER_NAME, userName)
        PostUtils.put(APIKey.USER_PASSWORD, password)
      } -> {
        val response = getWebContents(APIUri.LOGIN).onFailure { return it }
        if (response.isEmpty() || response.startsWith("Bad")) {
          Failure(LoginException("Failed to login: $response", IllegalStateException()))
        }
        sessionId = SessionId(response)
        /* FIXME:[PastebinAccount::login] change return type to something else and show in console/kak client */
        Success(sessionId.value)
      }
      else -> Failure(IllegalStateException("login failed"))
    }

  /**
   * Include all the following POST parameters when you request the url:
   *  1. api_dev_key - this is your API Developer Key, in your case: YOUR API DEVELOPER KEY
   *  2. api_user_key - this is the session key of the logged in user. How to obtain such a key
   *  3. api_option - set as 'userdetails'
   *
   *  Below is an example output of a user information listing:
   *   <user>
   *     <user_name>wiz_kitty</user_name>
   *     <user_format_short>text</user_format_short>
   *     <user_expiration>N</user_expiration>
   *     <user_avatar_url>https://pastebin.com/cache/a/1.jpg</user_avatar_url>
   *     <user_private>1</user_private> (0 Public, 1 Unlisted, 2 Private)
   *     <user_website>https://myawesomesite.com</user_website>
   *     <user_email>oh@dear.com</user_email>
   *     <user_location>New York</user_location>
   *     <user_account_type>1</user_account_type> (0 normal, 1 PRO)
   *  </user>
   *
   * Possible Bad API Responses:
   *  Bad API request, invalid api_option
   *  Bad API request, invalid api_dev_key
   *  Bad API request, invalid api_user_key
   */
  suspend fun getAccountDetails(): Result<PastebinAccountDetails, Exception> {
    return if (checkUserDetails()) Failure(ParseException("SessionId.isEmpty=${sessionId.value.isEmpty()} or DeveloperKey.isEmpty=${developerKey.value.isEmpty()}", IllegalStateException()))
    else when {
      putPostUtilsDevUserKey() && PostUtils.isSuccess { PostUtils.put(APIKey.OPTION, APIOption.USER_DETAILS) } -> {
        val response = getWebContents(APIUri.POST).onFailure { return it }
        if (response.startsWith("<user>")) {
          val document = DOMUtils.getDocument(response, APITag.USER).onFailure { return it }
          val element = document.item(0) as Element
          Success(PastebinAccountDetails(element))
        } else Failure(ParseException("Failed to parse account details: $response", IllegalStateException()))
      }
      else -> Failure(IllegalStateException())
    }
  }

  /**
   * Include all the following POST parameters when you request the url:
   *  1. api_dev_key - this is your API Developer Key, in your case: YOUR API DEVELOPER KEY
   *  2. api_user_key - this is the session key of the logged in user. How to obtain such a key
   *  3. api_results_limit - this is not required, by default its set to 50, min value is 1, max value is 1000
   *  4. api_option - set as 'list'
   *
   * Below is an example output of a users paste listing:
   *  <paste>
   *    <paste_key>0b42rwhf</paste_key>
   *    <paste_date>1297953260</paste_date>
   *    <paste_title>javascript test</paste_title>
   *    <paste_size>15</paste_size>
   *    <paste_expire_date>1297956860</paste_expire_date>
   *    <paste_private>0</paste_private>
   *    <paste_format_long>JavaScript</paste_format_long>
   *    <paste_format_short>javascript</paste_format_short>
   *    <paste_url>https://pastebin.com/0b42rwhf</paste_url>
   *    <paste_hits>15</paste_hits>
   *  </paste>
   *
   * Other Possible Good API Responses:
   *  No pastes found.
   * Possible Bad API Responses:
   *  Bad API request, invalid api_option
   *  Bad API request, invalid api_dev_key
   *  Bad API request, invalid api_user_key
   */
  /* FIXME:[PastebinAccount::getAccountPastes] imperative to functional */
  suspend fun getAccountPastes(limit: Limit): Result<List<UserPaste>, Exception> {
    if (checkUserDetails()) {
      return Failure(ParseException("Developer key or session id is missing", IllegalStateException()))
    }

    putPostUtilsDevUserKey()
    PostUtils.put(APIKey.RESULTS_LIMIT, limit.checkLimit())
    PostUtils.put(APIKey.OPTION, APIOption.LIST)

    val response = getWebContents(APIUri.POST).onFailure { return it }

    if (response.startsWith("No") || response.startsWith("Bad")) {
      return Failure(ParseException(response, NoSuchElementException()))
    }

    val pastes = mutableListOf<UserPaste>()

    if (response.startsWith("<paste>")) {
      val nodeList: NodeList = DOMUtils.getDocument(response, APITag.PASTE).onFailure { return it }

      (0 until nodeList.length).forEach { n ->
        val node = nodeList.item(n)
        if (node.nodeType == Node.ELEMENT_NODE) {
          val element = node as Element
          val result = UserPaste(
            """
              <paste>
                <paste_key>${XMLUtils.getText(element, PasteElement.KEY)}</paste_key>
                <paste_date>${XMLUtils.getText(element, PasteElement.DATE)}</paste_date>
                <paste_title>${XMLUtils.getText(element, PasteElement.TITLE)}</paste_title>
                <paste_size>${XMLUtils.getText(element, PasteElement.SIZE)}</paste_size>
                <paste_expire_date>${XMLUtils.getText(element, PasteElement.EXPIRE)}</paste_expire_date>
                <paste_private>${XMLUtils.getText(element, PasteElement.PRIVATE)}</paste_private>
                <paste_format_long>${XMLUtils.getText(element, PasteElement.FORMAT_LONG)}</paste_format_long>
                <paste_format_short>${XMLUtils.getText(element, PasteElement.FORMAT_SHORT)}</paste_format_short>
                <paste_url>${XMLUtils.getText(element, PasteElement.URL)}</paste_url>
                <paste_hits>${XMLUtils.getText(element, PasteElement.HITS)}</paste_hits>
              </paste>
            """.trimIndent()
          )
          pastes.add(result)
        }
      }
    }
    return Success(pastes)
  }

  suspend fun getAccountPastes(): Result<List<UserPaste>, Exception> =
    getAccountPastes(Limit("50"))

  suspend fun getAccountLatestPaste(): Result<List<UserPaste>, Exception> =
    getAccountPastes(Limit("1"))

  /**
   * Include all the following POST parameters when you request the url:
   *  1. api_dev_key - this is your API Developer Key, in your case: YOUR API DEVELOPER KEY
   *  2. api_user_key - this is the session key of the logged in user. How to obtain such a key
   *  3. api_paste_key - this is paste key you want to fetch the data from.
   *  4. api_option - set as 'show_paste'
   *
   * Possible Bad API Responses:
   *  Bad API request, invalid api_option
   *  Bad API request, invalid api_dev_key
   *  Bad API request, invalid api_user_key
   *  Bad API request, invalid permission to view this paste or invalid api_paste_key
   */
  suspend fun getAccountRawPastes(pasteKey: PasteKey): Result<String, Exception> =
    when {
      putPostUtilsDevUserKey() && PostUtils.isSuccess {
        PostUtils.put(APIKey.PASTE_KEY, pasteKey)
        PostUtils.put(APIKey.OPTION, APIOption.SHOW_PASTE)
      } -> {
        val response = getWebContents(APIUri.RAW).onFailure { return it }
        Success(response)
      }
      else -> Failure(IllegalStateException("getAccountRawPastes"))
    }

  /**
   * Include all the following POST parameters when you request the url:
   *  1. api_dev_key - this is your API Developer Key, in your case: YOUR API DEVELOPER KEY
   *  2. api_user_key - this is the session key of the logged in user. How to obtain such a key
   *  3. api_paste_key - this is the unique key of the paste you want to delete.
   *  4. api_option - set as 'delete'
   *
   * Possible Good API Responses:
   *  Paste Removed
   * Possible Bad API Responses:
   *  Bad API request, invalid api_option
   *  Bad API request, invalid api_dev_key
   *  Bad API request, invalid api_user_key
   *  Bad API request, invalid permission to remove paste
   */
  suspend fun delete(pasteKey: PasteKey): Result<String, Exception> {
    return when {
      PostUtils.isSuccess {
        PostUtils.clear()
        PostUtils.put(APIKey.DEV_KEY, developerKey)
        PostUtils.put(APIKey.USER_KEY, sessionId)
        PostUtils.put(APIKey.PASTE_KEY, pasteKey)
        PostUtils.put(APIKey.OPTION, APIOption.DELETE)
      } -> WebIO.getContents(APIUri.POST)
      else -> Failure(IllegalStateException("delete"))
    }
  }

  private fun checkAccountDetails(): Boolean {
    return sessionId.value.isNotBlank() ||
      userName.value.isBlank() ||
      password.value.isBlank() ||
      developerKey.value.isBlank()
  }

  private fun checkUserDetails(): Boolean {
    return sessionId.value.isEmpty() ||
      developerKey.value.isEmpty()
  }

  private fun putPostUtilsDevUserKey(): Boolean {
    return PostUtils.isSuccess {
      PostUtils.clear()
      PostUtils.put(APIKey.DEV_KEY, developerKey)
      PostUtils.put(APIKey.USER_KEY, sessionId)
    }
  }

  private suspend fun getWebContents(apiUri: APIUri): Result<String, Exception> {
    return WebIO.getContents(apiUri)
  }
}
