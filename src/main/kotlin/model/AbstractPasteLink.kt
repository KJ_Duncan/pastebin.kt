package model

import java.net.URL

abstract class AbstractPasteLink {
  abstract fun getLink(): URL
}
