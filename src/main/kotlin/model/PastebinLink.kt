package model

import api.PasteKey

import java.net.URL

import com.natpryce.Result
import com.natpryce.resultFrom

class PastebinLink(
  private val response: String
) : AbstractPasteLink() {
  /**
   * Possible Good API Responses: (example)
   *  https://pastebin.com/UIFdu235s
   *
   * Possible Bad API Responses:
   *  Bad API request, invalid api_option
   *  Bad API request, invalid api_dev_key
   *  Bad API request, IP blocked
   *  Bad API request, maximum number of 25 unlisted pastes for your free account
   *  Bad API request, maximum number of 10 private pastes for your free account
   *  Bad API request, api_paste_code was empty
   *  Bad API request, maximum paste file size exceeded
   *  Bad API request, invalid api_expire_date
   *  Bad API request, invalid api_paste_private
   *  Bad API request, invalid api_paste_format
   *  Bad API request, invalid api_user_key
   *  Bad API request, invalid or expired api_user_key
   */
  override fun getLink(): URL = URL(this.response)

  /* https://pastebin.com/UIFdu235s <- key */
  fun getKey(): Result<PasteKey, Exception> =
    resultFrom { PasteKey(getLink().path.substring(1)) }
}
