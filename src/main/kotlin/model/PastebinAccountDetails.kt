package model

import api.UserElement
import client.XMLUtils
import org.w3c.dom.Element

/**
 * Below is an example output of a user information listing:
 *  <user>
 *    <user_name>wiz_kitty</user_name>
 *    <user_format_short>text</user_format_short>
 *    <user_expiration>N</user_expiration>
 *    <user_avatar_url>https://pastebin.com/cache/a/1.jpg</user_avatar_url>
 *    <user_private>1</user_private> (0 Public, 1 Unlisted, 2 Private)
 *    <user_website>https://myawesomesite.com</user_website>
 *    <user_email>oh@dear.com</user_email>
 *    <user_location>New York</user_location>
 *    <user_account_type>1</user_account_type> (0 normal, 1 PRO)
 *  </user>
 */
class PastebinAccountDetails(element: Element) {
  private val username: String   = XMLUtils.getText(element, UserElement.NAME)
  private val format: String     = XMLUtils.getText(element, UserElement.FORMAT)
  private val expiration: String = XMLUtils.getText(element, UserElement.EXPIRATION)
  private val avatar: String     = XMLUtils.getText(element, UserElement.AVATAR)
  private val visibility: String = XMLUtils.getText(element, UserElement.PRIVATE)
  private val website: String    = XMLUtils.getText(element, UserElement.WEBSITE)
  private val email: String      = XMLUtils.getText(element, UserElement.EMAIL)
  private val location: String   = XMLUtils.getText(element, UserElement.LOCATION)
  private val account: String    = XMLUtils.getText(element, UserElement.ACCOUNT)

  val isPro: Boolean = account.toInt() == 1

  override fun toString(): String =
    """
      <user>
        <user_name>$username</user_name>
        <user_format_short>$format</user_format_short>
        <user_expiration>$expiration</user_expiration>
        <user_avatar_url>$avatar</user_avatar_url>
        <user_private>$visibility</user_private>
        <user_website>$website</user_website>
        <user_email>$email</user_email>
        <user_location>$location</user_location>
        <user_account_type>$account</user_account_type>
      </user>
    """.trimIndent()
}
