package model

import api.PasteCode
import com.natpryce.Result

abstract class AbstractPaste<P : AbstractPasteLink>(open val pasteCode: PasteCode) {
  abstract suspend fun paste(): Result<PastebinLink, Exception>
}
