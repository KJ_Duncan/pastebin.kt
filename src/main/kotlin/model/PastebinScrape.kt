package model

import api.FullUrl
import api.PasteDate
import api.PasteKey
import api.PasteName
import api.PasteSize
import api.ScrapeUrl
import api.UserName
import optional.PasteExpire
import optional.PasteFormat

data class PastebinScrape(
  val scrapeUrl: ScrapeUrl,
  val fullUrl: FullUrl,
  val pasteDate: PasteDate,
  val pasteKey: PasteKey,
  val pasteSize: PasteSize,
  val expireDate: PasteExpire,
  val pasteName: PasteName,
  val pasteFormat: PasteFormat,
  val pasteUserName: UserName
)
