package model

import api.APIKey
import api.APIOption
import api.APIUri
import api.DeveloperKey
import api.PasteCode
import api.PasteName
import api.SessionId
import client.PostUtils
import client.WebIO
import exception.PasteException
import optional.PasteExpire
import optional.PasteFormat
import optional.PasteVisibility

import com.natpryce.Failure
import com.natpryce.onFailure
import com.natpryce.Result
import com.natpryce.Success

/**
 * Pastebin API manual: http://pastebin.com/api/
 */
internal class PastebinPaste(
  private val developerKey: DeveloperKey,
  pasteCode: PasteCode,
  private val sessionId: SessionId     = SessionId(""),
  val pasteName: PasteName             = PasteName("PastebinAppKt"),
  val pasteFormat: PasteFormat         = PasteFormat.FORMAT_KOTLIN,
  val pasteVisibility: PasteVisibility = PasteVisibility.PUBLIC,
  val pasteExpire: PasteExpire         = PasteExpire.TEN_MINUTES
) : AbstractPaste<PastebinLink>(pasteCode) {

  override suspend fun paste(): Result<PastebinLink, Exception> {
    return when {
      pasteCode.value.isEmpty() -> {
        Failure(PasteException("Paste can not be empty", IllegalStateException()))
      }
      developerKey.value.isEmpty() -> {
        Failure(PasteException("Developer key is missing", IllegalStateException()))
      }
      else -> if (putPostUtils()) {
        val pageResponse = WebIO.getContents(APIUri.POST).onFailure { return it }
        Success(PastebinLink(pageResponse))
      } else {
        Failure(IllegalStateException("PostUtils.isSuccess=false"))
      }
    }
  }

  private fun putPostUtils(): Boolean {
    return PostUtils.isSuccess {
      PostUtils.clear()
      PostUtils.put(APIKey.OPTION, APIOption.PASTE)
      PostUtils.put(APIKey.DEV_KEY, developerKey)
      PostUtils.put(APIKey.USER_KEY, sessionId)
      PostUtils.put(APIKey.PASTE_CODE, pasteCode)    // abstract class open value
      PostUtils.put(APIKey.PASTE_NAME, pasteName)
      PostUtils.put(APIKey.PASTE_FORMAT, pasteFormat)
      PostUtils.put(APIKey.PASTE_PRIVATE, pasteVisibility)
      PostUtils.put(APIKey.PASTE_EXPIRES, pasteExpire)
    }
  }
}
