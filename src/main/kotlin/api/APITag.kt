package api

sealed class APITag {
  abstract val tag: String

  object PASTE : APITag() {
    override val tag: String = "paste"
  }

  object USER : APITag() {
    override val tag: String = "user"
  }

  override fun toString(): String = this.tag
}
