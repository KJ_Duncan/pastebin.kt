package api

sealed class APIKey {
  abstract val key: String

  object DEV_KEY : APIKey() {
    override val key: String = "api_dev_key"
  }

  object OPTION : APIKey() {
    override val key: String = "api_option"
  }

  object PASTE_CODE : APIKey() {
    override val key: String = "api_paste_code"
  }

  object PASTE_EXPIRES : APIKey() {
    override val key: String = "api_paste_expire_date"
  }

  object PASTE_FORMAT : APIKey() {
    override val key: String = "api_paste_format"
  }

  object PASTE_KEY : APIKey() {
    override val key: String = "api_paste_key"
  }

  object PASTE_NAME : APIKey() {
    override val key: String = "api_paste_name"
  }

  object PASTE_PRIVATE : APIKey() {
    override val key: String = "api_paste_private"
  }

  object RESULTS_LIMIT : APIKey() {
    override val key: String = "api_results_limit"
  }

  object USER_KEY : APIKey() {
    override val key: String = "api_user_key"
  }

  object USER_NAME : APIKey() {
    override val key: String = "api_user_name"
  }

  object USER_PASSWORD : APIKey() {
    override val key: String = "api_user_password"
  }

  override fun toString(): String = this.key
}
