package api

sealed class UserElement {
  abstract val element: String

  object ACCOUNT : UserElement() {
    override val element: String = "user_account_type"
  }

  object AVATAR : UserElement() {
    override val element: String = "user_avatar_url"
  }

  object EMAIL : UserElement() {
    override val element: String = "user_email"
  }

  object EXPIRATION : UserElement() {
    override val element: String = "user_expiration"
  }

  object FORMAT : UserElement() {
    override val element: String = "user_format_short"
  }

  object LOCATION : UserElement() {
    override val element: String = "user_location"
  }

  object NAME : UserElement() {
    override val element: String = "user_name"
  }

  object PRIVATE : UserElement() {
    override val element: String = "user_private"
  }

  object WEBSITE : UserElement() {
    override val element: String = "user_website"
  }

  override fun toString(): String = this.element
}
