package api

sealed class PasteElement {
  abstract val element: String

  object DATE : PasteElement() {
    override val element: String = "paste_date"
  }

  object EXPIRE : PasteElement() {
    override val element: String = "paste_expire_date"
  }

  object FORMAT_LONG : PasteElement() {
    override val element: String = "paste_format_long"
  }

  object FORMAT_SHORT : PasteElement() {
    override val element: String = "paste_format_short"
  }

  object HITS : PasteElement() {
    override val element: String = "paste_hits"
  }

  object KEY : PasteElement() {
    override val element: String = "paste_key"
  }

  object PRIVATE : PasteElement() {
    override val element: String = "paste_private"
  }

  object SIZE : PasteElement() {
    override val element: String = "paste_size"
  }

  object TITLE : PasteElement() {
    override val element: String = "paste_title"
  }

  object URL : PasteElement() {
    override val element: String = "paste_url"
  }

  override fun toString(): String = this.element
}
