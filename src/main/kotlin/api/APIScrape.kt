package api

sealed class APIScrape {
  abstract val value: String

  object DATE : APIScrape() {
    override val value: String = "date"
  }

  object EXPIRE : APIScrape() {
    override val value: String = "expire"
  }

  object FULL_URL : APIScrape() {
    override val value: String = "full_url"
  }

  object KEY : APIScrape() {
    override val value: String = "key"
  }

  object SCRAPE_URL : APIScrape() {
    override val value: String = "scrape_url"
  }

  object SIZE : APIScrape() {
    override val value: String = "size"
  }

  object SYNTAX : APIScrape() {
    override val value: String = "syntax"
  }

  object TITLE : APIScrape() {
    override val value: String = "title"
  }

  object USER : APIScrape() {
    override val value: String = "user"
  }

  override fun toString(): String = this.value
}
