package api

sealed class APIOption : APIValue {

  object DELETE : APIOption() {
    override val value: String = "delete"
  }

  object LIST : APIOption() {
    override val value: String = "list"
  }

  object PASTE : APIOption() {
    override val value: String = "paste"
  }

  object SHOW_PASTE : APIOption() {
    override val value: String = "show_paste"
  }

  object USER_DETAILS : APIOption() {
    override val value: String = "userdetails"
  }

  override fun toString(): String = this.value
}
