package api

interface APIValue {
  val value: String
}

@JvmInline value class DeveloperKey(override val value: String) : APIValue
@JvmInline value class FolderKey(override val value: String) : APIValue
@JvmInline value class FullUrl(override val value: String) : APIValue
@JvmInline value class Limit(override val value: String) : APIValue
@JvmInline value class Password(override val value: String) : APIValue
@JvmInline value class PasteCode(override val value: String) : APIValue
@JvmInline value class PasteDate(override val value: String) : APIValue
@JvmInline value class PasteHits(override val value: String) : APIValue
@JvmInline value class PasteKey(override val value: String) : APIValue
@JvmInline value class PasteName(override val value: String) : APIValue
@JvmInline value class PasteSize(override val value: String) : APIValue
@JvmInline value class ScrapeUrl(override val value: String) : APIValue
@JvmInline value class SessionId(override val value: String) : APIValue
@JvmInline value class UserName(override val value: String) : APIValue

@JvmInline value class Kakoune(val session: String)
@JvmInline value class UserPaste(val paste: String)

fun Limit.checkLimit(): Limit {
  val int = this.value.toInt()
  return if (int in 1..1000) this else Limit("50")
}
