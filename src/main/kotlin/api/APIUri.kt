package api

/**
 *  A hierarchical api.APIUri is subject to further parsing according to the syntax
 *  [scheme:][//authority][path][?query][#fragment]
 */
sealed class APIUri {
  abstract val uri: String

  object LOGIN : APIUri() {
    override val uri: String = "pastebin.com/api/api_login.php"
  }

  object META_SCRAPE : APIUri() {
    override val uri: String = "https://scrape.pastebin.com/api_scrape_item_meta.php"
  }

  object POST : APIUri() {
    override val uri: String = "pastebin.com/api/api_post.php"
  }

  object RAW : APIUri() {
    override val uri: String = "pastebin.com/api/api_raw.php"
  }

  object RAW_SCRAPE : APIUri() {
    override val uri: String = "https://scrape.pastebin.com/api_scrape_item.php"
  }

  object RECENT_SCRAPE : APIUri() {
    override val uri: String = "scrape.pastebin.com/api_scraping.php"
  }

  object SCHEME : APIUri() {
    override val uri: String = "https"
  }

  object toAPIUri : APIUri() {
    private var str: String = ""

    operator fun invoke(uri: String) {
      str = uri
    }

    override val uri: String = str
  }

  override fun toString(): String = this.uri
}
