import api.APIValue
import api.DeveloperKey
import api.Kakoune
import api.Limit
import api.Password
import api.PasteCode
import api.PasteKey
import api.PasteName
import api.SessionId
import api.UserName
import api.UserPaste
import client.ArkIO
import client.FileIO
import com.natpryce.Result
import com.natpryce.onFailure
import com.natpryce.valueOrNull
import domain.Pastebin
import domain.PastebinAccount
import kotlinx.coroutines.coroutineScope
import model.PastebinAccountDetails
import model.PastebinScrape
import optional.PasteExpire
import optional.PasteFormat
import optional.PasteVisibility
import java.net.URL

/**
 * PastebinPaste(developerKey, pasteCode, sessionId, pasteName, pasteFormat, pasteVisibility, pasteExpire)
 *
 * TODO: Implement CommandLine and CommandLineHandler to replace PastebinApp
 */
object PastebinApp {
  @JvmStatic
  suspend fun main(args: Array<String>): Unit = coroutineScope {
    /* TEST:[PastebinApp::main] passing runtime arguments with flag keep --alive */
    ArkIO.parseArguments(args)

    fun output(view: Result<URL, Exception>): Unit =
      if (ArkIO.session != null) FileIO(Kakoune(ArkIO.session!!), view) else FileIO(view)

    fun output(view: Result<PastebinAccountDetails, Exception>): Unit =
      if (ArkIO.session != null) FileIO(Kakoune(ArkIO.session!!), view) else FileIO(view)

    fun output(view: Result<String, Exception>): Unit =
      if (ArkIO.session != null) FileIO(Kakoune(ArkIO.session!!), view) else FileIO(view)

    fun output(view: Result<List<PastebinScrape>, Exception>): Unit =
      if (ArkIO.session != null) FileIO(Kakoune(ArkIO.session!!), view) else FileIO(view)

    fun output(view: Result<List<UserPaste>, Exception>): Unit =
      if (ArkIO.session != null) FileIO(Kakoune(ArkIO.session!!), view) else FileIO(view)

    val pbBuilder = listOf<APIValue>()
    val pbDev     = DeveloperKey(ArkIO.apiDevKey)
    val pbCode    = PasteCode(ArkIO.mainString)
    val pBin      = Pastebin()
    val pbAccount by lazy {
      PastebinAccount(
        developerKey = pbDev,
        userName     = UserName(ArkIO.apiUserName!!),
        password     = Password(ArkIO.apiUserPassword!!)
      )
    }
    val quit: (Int) -> Nothing = { kotlin.system.exitProcess(it) }

    suspend fun cliBuilder(sessionId: SessionId): Result<URL, Exception> =
      pBin.paste(
        developerKey    = pbDev,
        pasteCode       = pbCode,
        sessionId       = sessionId,
        pasteName       = pbBuilder[0] as PasteName,
        pasteFormat     = pbBuilder[1] as PasteFormat,
        pasteVisibility = pbBuilder[2] as PasteVisibility,
        pasteExpire     = pbBuilder[3] as PasteExpire
      )

    /* TEST:[PastebinApp::cliFlags] numerous patterns to test functionality and return types */
    /* TEST:[PastebinApp::cliFlags] ArkIO.visible?.let call with !! and return@let call */
    // if (ArkIO.visible != null) PasteVisibility.PUBLIC.lookup(ArkIO.visible!!) else PasteVisibility.PUBLIC,
    suspend fun cliFlags(sessionId: SessionId): Result<URL, Exception> =
      pBin.paste(
        developerKey    = pbDev,
        pasteCode       = pbCode,
        sessionId       = sessionId,
        pasteName       = if (ArkIO.title != null) PasteName(ArkIO.title!!) else PasteName("PastebinAppKt"),
        pasteFormat     = try { PasteFormat.valueOf("FORMAT_${ArkIO.format?.uppercase()}") } catch (e: IllegalArgumentException) { PasteFormat.FORMAT_KOTLIN },
        pasteVisibility = ArkIO.visible?.let { return@let PasteVisibility.PUBLIC.lookup(it) }!!,
        pasteExpire     = if (ArkIO.expire != null) PasteExpire.TEN_MINUTES.lookup(ArkIO.expire!!) else PasteExpire.TEN_MINUTES
      )

    if (ArkIO.builder != null) {
      pbBuilder + ArkIO.builder!!.let {
        return@let listOf(
          PasteName(it[0]),
          PasteFormat.valueOf("FORMAT_${it[1].uppercase()}"),
          PasteVisibility.valueOf(it[2].uppercase()),
          PasteExpire.TEN_MINUTES.lookup(it[3])
        )
      }
    }

    if (ArkIO.login) pbAccount.login().onFailure { println(it); quit(2) }

    if (ArkIO.results) output(pbAccount.getAccountPastes(Limit(ArkIO.mainString))).also { quit(1) }

    if (ArkIO.userDetails) output(pbAccount.getAccountDetails()).also { quit(1) }

    if (ArkIO.show) output(pbAccount.getAccountRawPastes(PasteKey(ArkIO.mainString))).also { quit(1) }

    if (ArkIO.delete) output(pbAccount.delete(PasteKey(ArkIO.mainString))).also { quit(1) }

    if (ArkIO.scrape) {
      pbAccount.getAccountDetails().valueOrNull()?.let {
        if (it.isPro) output(pBin.getScrapingContents(Limit(ArkIO.mainString)))
        else println("Pastebin account isPro=${it.isPro}")
      }.also { quit(1) }
    }

    val query: Result<URL, Exception> =
      when {
        pbBuilder.isNotEmpty() && ArkIO.login -> cliBuilder(pbAccount.sessionId)
        ArkIO.login -> cliFlags(pbAccount.sessionId)
        pbBuilder.isNotEmpty() -> cliBuilder(SessionId(""))
        else -> cliFlags(SessionId(""))
      }

    output(query).also { quit(1) }
  }
}
