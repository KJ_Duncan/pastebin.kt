package exception

import io.ktor.http.HttpStatusCode

class LoginException(
  override val message: String,
  override val cause: Throwable
) : Exception(message, cause)

class ParseException(
  override val message: String,
  override val cause: Throwable
) : Exception(message, cause)

class PasteException(
  override val message: String,
  override val cause: Throwable
) : Exception(message, cause)

class PostException(
  override val message: String,
  override val cause: Throwable
) : Exception(message, cause)

class PastebinError(
  val request: String,
  val code: HttpStatusCode,
  override val message: String,
  override val cause: Throwable
) : Error(message, cause)
