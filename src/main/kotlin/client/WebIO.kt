package client

import api.APIUri
import com.natpryce.Result
import com.natpryce.resultFrom
import io.ktor.client.HttpClient
import io.ktor.client.engine.cio.CIO
import io.ktor.client.request.post
import io.ktor.client.statement.HttpResponse
import io.ktor.client.statement.bodyAsText
import io.ktor.http.URLProtocol
import io.ktor.http.Url
import io.ktor.http.URLBuilder

object WebIO {

  suspend fun getContents(apiUri: APIUri): Result<String, Exception> =
    resultFrom {
      val client: HttpClient = HttpClient(CIO)
      val message: HttpResponse = client.post(urlBuilder(apiUri))
      client.close()
      message.bodyAsText()
    }

  private fun urlBuilder(apiUri: APIUri): Url {
    return URLBuilder(
      protocol = URLProtocol.HTTPS,
      host = apiUri.uri,
      port = URLProtocol.HTTPS.defaultPort,
      pathSegments = listOf<String>(),
      parameters = PostUtils.getPostParameters(),
      fragment = "",
      user = "",
      password = "",
      trailingQuery = false
    ).build()
  }
}
