package client

import api.APIKey
import api.APITag
import api.APIValue
import api.PasteElement
import api.UserElement
import com.natpryce.Result
import com.natpryce.resultFrom
import io.ktor.http.Parameters
import io.ktor.http.ParametersBuilder
import io.ktor.http.encodeURLParameter
import org.w3c.dom.Element
import org.w3c.dom.NodeList
import java.util.LinkedHashMap
import javax.xml.parsers.DocumentBuilderFactory

object DOMUtils {
  fun getDocument(response: String, apiTag: APITag): Result<NodeList, Exception> =
    resultFrom {
      val dbFactory = DocumentBuilderFactory.newInstance()
      val dBuilder = dbFactory.newDocumentBuilder()
      val document = dBuilder.parse(response)
      document.documentElement.normalize()
      document.getElementsByTagName(apiTag.tag)
    }
}

object XMLUtils {
  fun getText(parent: Element, tag: UserElement): String {
    return parent.getElementsByTagName(tag.element).item(0).textContent
  }

  fun getText(parent: Element, tag: PasteElement): String {
    return parent.getElementsByTagName(tag.element).item(0).textContent
  }
}

object PostUtils {
  /* Order matters, linkedHashMap preserves the order */
  private val post: LinkedHashMap<String, String> = linkedMapOf()

  fun put(apiKey: APIKey, apiValue: APIValue): String =
    post.put(apiKey.key.encodeURLParameter(), apiValue.value.encodeURLParameter()).orEmpty()

  fun clear(): Unit = post.clear()

  fun getPost(): Result<String, Exception> =
    resultFrom {
      buildString {
        post.forEach { (key, value) ->
          append(key)
          append("=")
          append(value)
          append("&")
        }
        ifEmpty { return@buildString }
        deleteCharAt(lastIndex)
      }
    }

  fun getPostParameters(): Parameters {
    if (post.isEmpty()) return Parameters.Empty

    val parametersBuilder = ParametersBuilder(post.size)
    post.forEach { (key, value) ->
      parametersBuilder.append(key, value)
    }
    return parametersBuilder.build()
  }

  inline fun isSuccess(op: () -> Unit): Boolean =
    try {
      op()
      true
    } catch (ex: Throwable) {
      /* TODO:[PostUtils::isSuccess] log exception message */
      // throw ParseException(ex.message.toString(), ex)
      false
    }
}
