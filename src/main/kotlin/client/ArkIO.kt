package client

import com.apurebase.arkenv.Arkenv
import com.apurebase.arkenv.configureArkenv
import com.apurebase.arkenv.feature.EnvironmentVariableFeature
import com.apurebase.arkenv.feature.ProfileFeature
import com.apurebase.arkenv.feature.PropertyFeature
import com.apurebase.arkenv.util.argument
import com.apurebase.arkenv.util.mainArgument

object ArkIO : Arkenv("PastebinAppKt", configureArkenv {
  uninstall(ProfileFeature())
  uninstall(PropertyFeature())
  uninstall(EnvironmentVariableFeature())
}) {

  val apiDevKey: String by argument()
  val apiUserName: String? by argument()
  val apiUserPassword: String? by argument()

  /* NOTE:[Arkenv] <https://apurebase.gitlab.io/arkenv/features/command-line/> nullable types do not have to be declared */
  val session: String? by argument("-s") {
    description = "The Kakoune session \$kak_session"
  }

  val title: String? by argument("-t") {
    description = "Title of your paste, default: PastebinAppKt"
  }

  val format: String? by argument("-f") {
    description = "Syntax highlighting value, default: Kotlin"
  }

  val visible: String? by argument("-v") {
    validate("Options are: public|unlisted|private")
    { it.contains("(public|unlisted|private)".toRegex()) }
    description = "public, unlisted, private implies --login, default: public"
  }

  val expire: String? by argument("-e") {
    validate("Options are: 10M|1H|1D|1W|1M|1Y|2W|6M")
    { it.contains("(10M|1H|1D|1W|1M|1Y|2W|6M)".toRegex()) }
    description = "Set the expiration date of your paste, default: 10M"
  }

  val builder: List<String>? by argument("-b") {
    description = "Pastebin builder: title|format|visible|expire"
    mapping = { it.split("|") }
  }

  val login: Boolean by argument("-L") {
    description = "Login to your pastebin account"
  }

  val results: Boolean by argument("-r") {
    description = "Number of pastes to list for current user max 1000, implies --login, default: 50"
  }

  val delete: Boolean by argument("-d") {
    description = "Unique paste key you want to delete, implies --login"
  }

  val userDetails: Boolean by argument("-u", "--user") {
    description = "Details of the current user, implies --login, default: false"
  }

  val show: Boolean by argument {
    description = "Raw paste output from unique paste key, implies --login, default: false"
  }

  val scrape: Boolean by argument {
    description = "Scape Pastebin, default: false"
  }

  val mainString: String by mainArgument {
    description = "The paste to upload is the last argument or as \$kak_selection"
  }
}
