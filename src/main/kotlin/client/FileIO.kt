package client

import api.Kakoune
import api.UserPaste
import com.natpryce.Result
import com.natpryce.peek
import model.PastebinAccountDetails
import model.PastebinScrape
import java.net.URL
import java.nio.file.Path
import java.util.concurrent.TimeUnit
import kotlin.io.path.appendText
import kotlin.io.path.createTempFile

/* FIXME:[FileIO::class] will create tmp file and open in kakoune client */
object FileIO {
  private var kakoune: Kakoune = Kakoune("")

  /* TEST:[FileIO::tempFile] does it create a new file each time? or uses the same one? */
  private val tempFile: Lazy<Path> = lazy {
    kotlin.runCatching {
      createTempFile("kak-pastebinkt-")
    }.getOrThrow()
  }

  private val processBuilder: Unit by lazy {
    with(ProcessBuilder()) {
      command(
        listOf(
          "echo 'edit! -existing ${tempFile.value.toAbsolutePath()} \n hook global KakEnd .* %{ nop %sh{ rm ${tempFile.value.toAbsolutePath()} } }' | kak -p ${kakoune.session}"
        )
      )
    }.start().also { process ->
      process.waitFor(5L, TimeUnit.SECONDS)
    }.destroy()
  }

  operator fun invoke(output: Result<URL, Exception>) {
    output.peek { tempFile.value.appendText(it.readText()) }
  }

  operator fun invoke(
    output: Result<PastebinAccountDetails, Exception>,
    satu: Unit = Unit
  ) {
    output.peek { tempFile.value.appendText(it.toString()) }
  }

  operator fun invoke(
    output: Result<String, Exception>,
    satu: Unit = Unit,
    dua: Unit = Unit
  ) {
    output.peek { tempFile.value.appendText(it) }
  }

  operator fun invoke(
    output: Result<List<PastebinScrape>, Exception>,
    satu: Unit = Unit,
    dua: Unit = Unit,
    tiga: Unit = Unit
  ) {
    output.peek { list ->
      list.forEach { scrape ->
        tempFile.value.appendText(scrape.toString())
      }
    }
  }

  operator fun invoke(
    output: Result<List<UserPaste>, Exception>,
    satu: Unit = Unit,
    dua: Unit = Unit,
    tiga: Unit = Unit,
    empat: Unit = Unit
  ) {
    output.peek { list ->
      list.forEach { user ->
        tempFile.value.appendText(user.paste)
      }
    }
  }

  operator fun invoke(
    kakoune: Kakoune,
    output: Result<URL, Exception>
  ) {
    output.peek { tempFile.value.appendText(it.readText()) }
    this.kakoune = kakoune
    run { processBuilder }
  }

  operator fun invoke(
    kakoune: Kakoune,
    output: Result<PastebinAccountDetails, Exception>,
    satu: Unit = Unit
  ) {
    output.peek { tempFile.value.appendText(it.toString()) }
    this.kakoune = kakoune
    run { processBuilder }
  }

  operator fun invoke(
    kakoune: Kakoune,
    output: Result<String, Exception>,
    satu: Unit = Unit,
    dua: Unit = Unit
  ) {
    output.peek { tempFile.value.appendText(it) }
    this.kakoune = kakoune
    run { processBuilder }
  }

  operator fun invoke(
    kakoune: Kakoune,
    output: Result<List<PastebinScrape>, Exception>,
    satu: Unit = Unit,
    dua: Unit = Unit,
    tiga: Unit = Unit
  ) {
    output.peek { list ->
      list.forEach { scrape ->
        tempFile.value.appendText(scrape.toString())
      }
    }
    this.kakoune = kakoune
    run { processBuilder }
  }

  operator fun invoke(
    kakoune: Kakoune,
    output: Result<List<UserPaste>, Exception>,
    satu: Unit = Unit,
    dua: Unit = Unit,
    tiga: Unit = Unit,
    empat: Unit = Unit
  ) {
    output.peek { list ->
      list.forEach { user ->
        tempFile.value.appendText(user.paste)
      }
    }
    this.kakoune = kakoune
    run { processBuilder }
  }
}
