package optional

import api.APIValue

import kotlin.time.Duration.Companion.days
import kotlin.time.Duration.Companion.hours
import kotlin.time.Duration.Companion.minutes
import kotlin.time.DurationUnit.SECONDS

/**
 * We have 9 valid values available which you can use with the api_paste_expire_date parameter:
 *  N = Never
 *  10M = 10 Minutes
 *  1H = 1 Hour
 *  1D = 1 Day
 *  1W = 1 Week
 *  2W = 2 Weeks
 *  1M = 1 Month
 *  6M = 6 Months
 *  1Y = 1 Year
 */
enum class PasteExpire(private val seconds: Int, override val value: String) : APIValue {
  NEVER(0, "N"),
  TEN_MINUTES((10.minutes).toInt(SECONDS), "10M"),
  ONE_HOUR((60.minutes).toInt(SECONDS), "1H"),
  ONE_DAY((24.hours).toInt(SECONDS), "1D"),
  ONE_WEEK((7.days).toInt(SECONDS), "1W"),
  TWO_WEEKS((14.days).toInt(SECONDS), "2W"),
  ONE_MONTH((30.days).toInt(SECONDS), "1M"),
  SIX_MONTHS((180.days).toInt(SECONDS), "6M"),
  ONE_YEAR((360.days).toInt(SECONDS), "1Y");

  fun expires(timeSeconds: Int): PasteExpire {
    values().forEach { time ->
      if (time.seconds == timeSeconds) return time
    }
    return ONE_MONTH
  }

  fun lookup(expire: String): PasteExpire {
    values().forEach { pe ->
      if (pe.value == expire.uppercase()) return pe
    }
    return TEN_MINUTES
  }

  override fun toString(): String = this.value
}

