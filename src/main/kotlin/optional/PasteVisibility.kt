package optional

import api.APIValue

/**
 * We have 3 valid values available which you can use with the api_paste_private parameter:
 *  0 = Public
 *  1 = Unlisted
 *  2 = Private
 *        is only allowed in combination with api_user_key,
 *        as you have to be logged into your account to access the paste.
 */
enum class PasteVisibility(override val value: String) : APIValue {
  PUBLIC("0"),
  UNLISTED("1"),
  PRIVATE("2");

  fun lookup(visible: String): PasteVisibility {
    values().forEach { pv ->
      if (pv.name.lowercase() == visible.lowercase() || pv.value == visible) return pv
    }
    return PUBLIC
  }
}
