package optional

import api.APIValue

// https://github.com/six519/PastebinPython/blob/master/pastebin_python/pastebin_formats.py
enum class PasteFormat : APIValue {
  FORMAT_4CS {
    override val value: String = "4cs"
  },
  FORMAT_6502_ACME_CROSS_ASSEMBLER {
    override val value: String = "6502acme"
  },
  FORMAT_6502_KICK_ASSEMBLER {
    override val value: String = "6502kickass"
  },
  FORMAT_6502_TASM_64TASS {
    override val value: String = "6502tasm"
  },
  FORMAT_ABAP {
    override val value: String = "abap"
  },
  FORMAT_ACTIONSCRIPT {
    override val value: String = "actionscript"
  },
  FORMAT_ACTIONSCRIPT_3 {
    override val value: String = "actionscript3"
  },
  FORMAT_ADA {
    override val value: String = "ada"
  },
  FORMAT_ALGOL_68 {
    override val value: String = "algol68"
  },
  FORMAT_APACHE_LOG {
    override val value: String = "apache"
  },
  FORMAT_APPLESCRIPT {
    override val value: String = "applescript"
  },
  FORMAT_APT_SOURCES {
    override val value: String = "apt_sources"
  },
  FORMAT_ARM {
    override val value: String = "arm"
  },
  FORMAT_ASM_NASM {
    override val value: String = "asm"
  },
  FORMAT_ASP {
    override val value: String = "asp"
  },
  FORMAT_ASYMPTOTE {
    override val value: String = "asymptote"
  },
  FORMAT_AUTOCONF {
    override val value: String = "autoconf"
  },
  FORMAT_AUTOHOTKEY {
    override val value: String = "autohotkey"
  },
  FORMAT_AUTOIT {
    override val value: String = "autoit"
  },
  FORMAT_AVISYNTH {
    override val value: String = "avisynth"
  },
  FORMAT_AWK {
    override val value: String = "awk"
  },
  FORMAT_BASCOM_AVR {
    override val value: String = "bascomavr"
  },
  FORMAT_BASH {
    override val value: String = "bash"
  },
  FORMAT_BASIC4GL {
    override val value: String = "basic4gl"
  },
  FORMAT_BIBTEX {
    override val value: String = "bibtex"
  },
  FORMAT_BLITZ_BASIC {
    override val value: String = "blitzbasic"
  },
  FORMAT_BNF {
    override val value: String = "bnf"
  },
  FORMAT_BOO {
    override val value: String = "boo"
  },
  FORMAT_BRAINFUCK {
    override val value: String = "bf"
  },
  FORMAT_C {
    override val value: String = "c"
  },
  FORMAT_C_FOR_MACS {
    override val value: String = "c_mac"
  },
  FORMAT_C_INTERMEDIATE_LANGUAGE {
    override val value: String = "cil"
  },
  FORMAT_C_SHARP {
    override val value: String = "csharp"
  },
  FORMAT_CPP {
    override val value: String = "cpp"
  },
  FORMAT_CPP_WITH_QT {
    override val value: String = "cpp-qt"
  },
  FORMAT_C_LOADRUNNER {
    override val value: String = "c_loadrunner"
  },
  FORMAT_CAD_DCL {
    override val value: String = "caddcl"
  },
  FORMAT_CAD_LISP {
    override val value: String = "cadlisp"
  },
  FORMAT_CFDG {
    override val value: String = "cfdg"
  },
  FORMAT_CHAISCRIPT {
    override val value: String = "chaiscript"
  },
  FORMAT_CLOJURE {
    override val value: String = "clojure"
  },
  FORMAT_CLONE_C {
    override val value: String = "klonec"
  },
  FORMAT_CLONE_CPP {
    override val value: String = "klonecpp"
  },
  FORMAT_CMAKE {
    override val value: String = "cmake"
  },
  FORMAT_COBOL {
    override val value: String = "cobol"
  },
  FORMAT_COFFEESCRIPT {
    override val value: String = "coffeescript"
  },
  FORMAT_COLDFUSION {
    override val value: String = "cfm"
  },
  FORMAT_CSS {
    override val value: String = "css"
  },
  FORMAT_CUESHEET {
    override val value: String = "cuesheet"
  },
  FORMAT_D {
    override val value: String = "d"
  },
  FORMAT_DCL {
    override val value: String = "dcl"
  },
  FORMAT_DCPU_16 {
    override val value: String = "dcpu16"
  },
  FORMAT_DCS {
    override val value: String = "dcs"
  },
  FORMAT_DELPHI {
    override val value: String = "delphi"
  },
  FORMAT_DELPHI_PRISM_OXYGENE {
    override val value: String = "oxygene"
  },
  FORMAT_DIFF {
    override val value: String = "diff"
  },
  FORMAT_DIV {
    override val value: String = "div"
  },
  FORMAT_DOS {
    override val value: String = "dos"
  },
  FORMAT_DOT {
    override val value: String = "dot"
  },
  FORMAT_E {
    override val value: String = "e"
  },
  FORMAT_ECMASCRIPT {
    override val value: String = "ecmascript"
  },
  FORMAT_EIFFEL {
    override val value: String = "eiffel"
  },
  FORMAT_EMAIL {
    override val value: String = "email"
  },
  FORMAT_EPC {
    override val value: String = "epc"
  },
  FORMAT_ERLANG {
    override val value: String = "erlang"
  },
  FORMAT_F_SHARP {
    override val value: String = "fsharp"
  },
  FORMAT_FALCON {
    override val value: String = "falcon"
  },
  FORMAT_FO_LANGUAGE {
    override val value: String = "fo"
  },
  FORMAT_FORMULA_ONE {
    override val value: String = "f1"
  },
  FORMAT_FORTRAN {
    override val value: String = "fortran"
  },
  FORMAT_FREEBASIC {
    override val value: String = "freebasic"
  },
  FORMAT_FREESWITCH {
    override val value: String = "freeswitch"
  },
  FORMAT_GAMBAS {
    override val value: String = "gambas"
  },
  FORMAT_GAME_MAKER {
    override val value: String = "gml"
  },
  FORMAT_GDB {
    override val value: String = "gdb"
  },
  FORMAT_GENERO {
    override val value: String = "genero"
  },
  FORMAT_GENIE {
    override val value: String = "genie"
  },
  FORMAT_GETTEXT {
    override val value: String = "gettext"
  },
  FORMAT_GO {
    override val value: String = "go"
  },
  FORMAT_GROOVY {
    override val value: String = "groovy"
  },
  FORMAT_GWBASIC {
    override val value: String = "gwbasic"
  },
  FORMAT_HASKELL {
    override val value: String = "haskell"
  },
  FORMAT_HAXE {
    override val value: String = "haxe"
  },
  FORMAT_HICEST {
    override val value: String = "hicest"
  },
  FORMAT_HQ9_PLUS {
    override val value: String = "hq9plus"
  },
  FORMAT_HTML {
    override val value: String = "html4strict"
  },
  FORMAT_HTML_5 {
    override val value: String = "html5"
  },
  FORMAT_ICON {
    override val value: String = "icon"
  },
  FORMAT_IDL {
    override val value: String = "idl"
  },
  FORMAT_INI_FILE {
    override val value: String = "ini"
  },
  FORMAT_INNO_SCRIPT {
    override val value: String = "inno"
  },
  FORMAT_INTERCAL {
    override val value: String = "intercal"
  },
  FORMAT_IO {
    override val value: String = "io"
  },
  FORMAT_J {
    override val value: String = "j"
  },
  FORMAT_JAVA {
    override val value: String = "java"
  },
  FORMAT_JAVA_5 {
    override val value: String = "java5"
  },
  FORMAT_JAVASCRIPT {
    override val value: String = "javascript"
  },
  FORMAT_JQUERY {
    override val value: String = "jquery"
  },
  FORMAT_KIXTART {
    override val value: String = "kixtart"
  },
  FORMAT_KOTLIN {
    override val value: String = "kotlin"
  },
  FORMAT_LATEX {
    override val value: String = "latex"
  },
  FORMAT_LDIF {
    override val value: String = "ldif"
  },
  FORMAT_LIBERTY_BASIC {
    override val value: String = "lb"
  },
  FORMAT_LINDEN_SCRIPTING {
    override val value: String = "lsl2"
  },
  FORMAT_LISP {
    override val value: String = "lisp"
  },
  FORMAT_LLVM {
    override val value: String = "llvm"
  },
  FORMAT_LOCO_BASIC {
    override val value: String = "locobasic"
  },
  FORMAT_LOGTALK {
    override val value: String = "logtalk"
  },
  FORMAT_LOL_CODE {
    override val value: String = "lolcode"
  },
  FORMAT_LOTUS_FORMULAS {
    override val value: String = "lotusformulas"
  },
  FORMAT_LOTUS_SCRIPT {
    override val value: String = "lotusscript"
  },
  FORMAT_LSCRIPT {
    override val value: String = "lscript"
  },
  FORMAT_LUA {
    override val value: String = "lua"
  },
  FORMAT_M68000_ASSEMBLER {
    override val value: String = "m68k"
  },
  FORMAT_MAGIKSF {
    override val value: String = "magiksf"
  },
  FORMAT_MAKE {
    override val value: String = "make"
  },
  FORMAT_MAPBASIC {
    override val value: String = "mapbasic"
  },
  FORMAT_MATLAB {
    override val value: String = "matlab"
  },
  FORMAT_MIRC {
    override val value: String = "mirc"
  },
  FORMAT_MIX_ASSEMBLER {
    override val value: String = "mmix"
  },
  FORMAT_MODULA_2 {
    override val value: String = "modula2"
  },
  FORMAT_MODULA_3 {
    override val value: String = "modula3"
  },
  FORMAT_MOTOROLA_68000_HISOFT_DEV {
    override val value: String = "68000devpac"
  },
  FORMAT_MPASM {
    override val value: String = "mpasm"
  },
  FORMAT_MXML {
    override val value: String = "mxml"
  },
  FORMAT_MYSQL {
    override val value: String = "mysql"
  },
  FORMAT_NAGIOS {
    override val value: String = "nagios"
  },
  FORMAT_NEWLISP {
    override val value: String = "newlisp"
  },
  FORMAT_NONE {
    override val value: String = "text"
  },
  FORMAT_NULLSOFT_INSTALLER {
    override val value: String = "nsis"
  },
  FORMAT_OBERON_2 {
    override val value: String = "oberon2"
  },
  FORMAT_OBJECK_PROGRAMMING_LANGUA {
    override val value: String = "objeck"
  },
  FORMAT_OBJECTIVE_C {
    override val value: String = "objc"
  },
  FORMAT_OCALM_BRIEF {
    override val value: String = "ocaml-brief"
  },
  FORMAT_OCAML {
    override val value: String = "ocaml"
  },
  FORMAT_OCTAVE {
    override val value: String = "octave"
  },
  FORMAT_OPENBSD_PACKET_FILTER {
    override val value: String = "pf"
  },
  FORMAT_OPENGL_SHADING {
    override val value: String = "glsl"
  },
  FORMAT_OPENOFFICE_BASIC {
    override val value: String = "oobas"
  },
  FORMAT_ORACLE_11 {
    override val value: String = "oracle11"
  },
  FORMAT_ORACLE_8 {
    override val value: String = "oracle8"
  },
  FORMAT_OZ {
    override val value: String = "oz"
  },
  FORMAT_PARASAIL {
    override val value: String = "parasail"
  },
  FORMAT_PARI_GP {
    override val value: String = "parigp"
  },
  FORMAT_PASCAL {
    override val value: String = "pascal"
  },
  FORMAT_PAWN {
    override val value: String = "pawn"
  },
  FORMAT_PCRE {
    override val value: String = "pcre"
  },
  FORMAT_PER {
    override val value: String = "per"
  },
  FORMAT_PERL {
    override val value: String = "perl"
  },
  FORMAT_PERL_6 {
    override val value: String = "perl6"
  },
  FORMAT_PHP {
    override val value: String = "php"
  },
  FORMAT_PHP_BRIEF {
    override val value: String = "php-brief"
  },
  FORMAT_PIC_16 {
    override val value: String = "pic16"
  },
  FORMAT_PIKE {
    override val value: String = "pike"
  },
  FORMAT_PIXEL_BENDER {
    override val value: String = "pixelbender"
  },
  FORMAT_PL_SQL {
    override val value: String = "plsql"
  },
  FORMAT_POSTGRESQL {
    override val value: String = "postgresql"
  },
  FORMAT_POV_RAY {
    override val value: String = "povray"
  },
  FORMAT_POWER_SHELL {
    override val value: String = "powershell"
  },
  FORMAT_POWERBUILDER {
    override val value: String = "powerbuilder"
  },
  FORMAT_PROFTPD {
    override val value: String = "proftpd"
  },
  FORMAT_PROGRESS {
    override val value: String = "progress"
  },
  FORMAT_PROLOG {
    override val value: String = "prolog"
  },
  FORMAT_PROPERTIES {
    override val value: String = "properties"
  },
  FORMAT_PROVIDEX {
    override val value: String = "providex"
  },
  FORMAT_PUREBASIC {
    override val value: String = "purebasic"
  },
  FORMAT_PYCON {
    override val value: String = "pycon"
  },
  FORMAT_PYTHON {
    override val value: String = "python"
  },
  FORMAT_PYTHON_FOR_S60 {
    override val value: String = "pys60"
  },
  FORMAT_Q_KDB_PLUS {
    override val value: String = "q"
  },
  FORMAT_QBASIC {
    override val value: String = "qbasic"
  },
  FORMAT_R {
    override val value: String = "rsplus"
  },
  FORMAT_RAILS {
    override val value: String = "rails"
  },
  FORMAT_REBOL {
    override val value: String = "rebol"
  },
  FORMAT_REG {
    override val value: String = "reg"
  },
  FORMAT_REXX {
    override val value: String = "rexx"
  },
  FORMAT_ROBOTS {
    override val value: String = "robots"
  },
  FORMAT_RPM_SPEC {
    override val value: String = "rpmspec"
  },
  FORMAT_RUBY {
    override val value: String = "ruby"
  },
  FORMAT_RUBY_GNUPLOT {
    override val value: String = "gnuplot"
  },
  FORMAT_SAS {
    override val value: String = "sas"
  },
  FORMAT_SCALA {
    override val value: String = "scala"
  },
  FORMAT_SCHEME {
    override val value: String = "scheme"
  },
  FORMAT_SCILAB {
    override val value: String = "scilab"
  },
  FORMAT_SDLBASIC {
    override val value: String = "sdlbasic"
  },
  FORMAT_SMALLTALK {
    override val value: String = "smalltalk"
  },
  FORMAT_SMARTY {
    override val value: String = "smarty"
  },
  FORMAT_SPARK {
    override val value: String = "spark"
  },
  FORMAT_SPARQL {
    override val value: String = "sparql"
  },
  FORMAT_SQL {
    override val value: String = "sql"
  },
  FORMAT_STONESCRIPT {
    override val value: String = "stonescript"
  },
  FORMAT_SYSTEMVERILOG {
    override val value: String = "systemverilog"
  },
  FORMAT_T_SQL {
    override val value: String = "tsql"
  },
  FORMAT_TCL {
    override val value: String = "tcl"
  },
  FORMAT_TERA_TERM {
    override val value: String = "teraterm"
  },
  FORMAT_THINBASIC {
    override val value: String = "thinbasic"
  },
  FORMAT_TYPOSCRIPT {
    override val value: String = "typoscript"
  },
  FORMAT_UNICON {
    override val value: String = "unicon"
  },
  FORMAT_UNREALSCRIPT {
    override val value: String = "uscript"
  },
  FORMAT_UPC {
    override val value: String = "ups"
  },
  FORMAT_URBI {
    override val value: String = "urbi"
  },
  FORMAT_VALA {
    override val value: String = "vala"
  },
  FORMAT_VB_NET {
    override val value: String = "vbnet"
  },
  FORMAT_VEDIT {
    override val value: String = "vedit"
  },
  FORMAT_VERILOG {
    override val value: String = "verilog"
  },
  FORMAT_VHDL {
    override val value: String = "vhdl"
  },
  FORMAT_VIM {
    override val value: String = "vim"
  },
  FORMAT_VISUAL_PRO_LOG {
    override val value: String = "visualprolog"
  },
  FORMAT_VISUALBASIC {
    override val value: String = "vb"
  },
  FORMAT_VISUALFOXPRO {
    override val value: String = "visualfoxpro"
  },
  FORMAT_WHITESPACE {
    override val value: String = "whitespace"
  },
  FORMAT_WHOIS {
    override val value: String = "whois"
  },
  FORMAT_WINBATCH {
    override val value: String = "winbatch"
  },
  FORMAT_XBASIC {
    override val value: String = "xbasic"
  },
  FORMAT_XML {
    override val value: String = "xml"
  },
  FORMAT_XORG_CONFIG {
    override val value: String = "xorg_conf"
  },
  FORMAT_XPP {
    override val value: String = "xpp"
  },
  FORMAT_YAML {
    override val value: String = "yaml"
  },
  FORMAT_Z80_ASSEMBLER {
    override val value: String = "z80"
  },
  FORMAT_ZXBASIC {
    override val value: String = "zxbasic"
  };

  fun format(fmt: String): PasteFormat {
    values().forEach { pbFormat ->
      if (pbFormat.value == fmt.lowercase()) return pbFormat
    }
    return FORMAT_KOTLIN
  }
}
