### PastebinKt a command line application and Kakoune editor integration 

  
NOTE Outstanding tasks coincide with [mailer](https://bitbucket.org/KJ_Duncan/mailer.kt/src/master/). Active research, development, and implementation is occurring and will be transposed into this application after unit testing.  
  
Not yet production ready, outstanding tasks are:  
  
+ Requirement models
+ Design models
+ Test suite non exhaustive
+ Method/Function refactoring 
+ Docstrings and References

---

![](src/main/resources/img/usecase.png)

