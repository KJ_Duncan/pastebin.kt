#### PastebinAccount ####

```kotlin

private fun pasteVisibility(element: Element): PasteVisibility {
  return when (XMLUtils.getText(element, PasteElement.PRIVATE)) {
    PasteVisibility.PUBLIC.value -> PasteVisibility.PUBLIC
    PasteVisibility.UNLISTED.value -> PasteVisibility.UNLISTED
    else -> PasteVisibility.PRIVATE
  }
}

private fun pasteExpireDate(expire: String, date: String): PasteExpire {
  val expireDate = expire.toLong()
  val pasteDate = date.toLong()
  return if (expireDate == 0L) PasteExpire.NEVER
  else PasteExpire.NEVER.expires((expireDate - pasteDate).toInt())
}

```

---

#### PastebinApp ####


```kotlin

/* TEST:[PastebinApp::cliFlags] ArkIO.visible?.let call with !! and return@let call */

if (ArkIO.title != null) PasteName(ArkIO.title!!) else PasteName("PastebinAppKt")
try { PasteFormat.valueOf("FORMAT_${ArkIO.format?.toUpperCase()}") } catch (e: IllegalArgumentException) { PasteFormat.FORMAT_KOTLIN }
ArkIO.visible?.let { return@let PasteVisibility.PUBLIC.lookup(it) }!!
if (ArkIO.visible != null) PasteVisibility.PUBLIC.lookup(ArkIO.visible!!) else PasteVisibility.PUBLIC
if (ArkIO.expire != null) PasteExpire.TEN_MINUTES.lookup(ArkIO.expire!!) else PasteExpire.TEN_MINUTES

```

---

#### PostUtils ####


```kotlin

/**
* NOTE: https://github.com/ktorio/ktor-samples/blob/master/other/structured-logging/src/StructuredLogging.kt
* Attaches a [key] named context object [value] temporally while the [callback] is executed.
* This function is inline so can be used in suspend functions respecting the.
*/
private val attributes = linkedMapOf<String, String>()

fun attach(key: String, value: String, callback: () -> Unit) {
  val oldValue = attributes[key]
  attributes[key] = value
  try {
    callback()
  } finally {
    attributes.putOrRemove(key, oldValue)
  }
}

/**
* Utility function to either [MutableMap.remove] or [MutableMap.put] a [key]/[value] from a [MutableMap]
* depending on the nullability of the value.
*/
private fun <K, V> MutableMap<K, V>.putOrRemove(key: K, value: V?): Any? {
  return when (value) {
    null -> this.remove(key)
    else -> this[key] = value
  }
}

```

---

#### CLI Flags ####

A couple of bash scripts to provide the user with base case functionality,  
place in hidden .folder not on your $PATH and your done.  
No requirements to use environment features, properties, or config files.  
  
PastebinApp::main how am I going to take args and paste? With bash scripts.  
*   java -jar PastebinApp.jar -s %val{kak_session} <all other args> %val{kak_selection}  
*   java PastebinAppKt -l -b title|format|visible|expire $kak_selection  
*   java PastebinAppKt -al $kak_selection  
   
  
```shell
#!/usr/bin/env bash
java pastebinkt -L -dev=WWWW -user=XXXX -pass=YYYY "$@"
```
  
```shell
#!/usr/bin/env bash
java pastebinkt @/path/to/file/args

# picocli demo expands to
java pastebinkt ABC -option=123 "X Y Z"
```

```
#! /usr/bin/env kscript
val file = File("path/to/config/mycommand.properties")
file.parse { Regex(it)  }
```

java -jar PastebinAppKt.jar --arkenv_profile private  
  
always=api_dev_key  
  
api_paste_private=0 (public)  
api_paste_private=1 (unlisted)  
  
api_paste_private=2 (private)  
required=api_user_name  
required=api_user_password  
  

CommandLine.ExitCode.OK(0)  
CommandLine.ExitCode.SOFTWARE(1)  
CommandLine.ExitCode.USAGE(2)  


---

#### TODO ####

```kotlin
/* 
 * TODO: 
 *   Mockk response calls to the Pastebin API,
 *   XML/DOM-Utils Node and Element testing (check fasterxml tests for use-cases), 
 *   Kakoune client UI (tempFile and Exceptions),
 *   Ark command line tests,
 *   Ark command line --keep-alive,
 *   Docstrings,
 *   Tests,
 *   Coverage,
 *   Check visability modifiers package/class/methods/fields,
 *   PastebinAccount refactor imperative to functional style coding,
 */
```
